/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.kingston.ku63026.fsdswingeventlisteners.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import uk.ac.kingston.ku63026.fsdswingeventlisteners.model.Organisation;

/**
 *
 * @author dave
 */
public class ButtonPanel extends JPanel implements ActionListener {
    public JButton readFile = new JButton("Read Staff File");
    public JButton quit = new JButton("Quit");

    public ButtonPanel() {
        this.setLayout(new GridLayout(1,2));
        this.readFile.addActionListener(this);
        this.readFile.setActionCommand("Read File");
        this.quit.addActionListener(this);
        this.quit.setActionCommand("Quit");
        this.add(readFile);
        this.add(quit);
    }

    public JButton getReadFile() {
        return readFile;
    }

    public void setReadFile(JButton readFile) {
        this.readFile = readFile;
    }

    public JButton getQuit() {
        return quit;
    }

    public void setQuit(JButton quit) {
        this.quit = quit;
    }

     @Override
    public void actionPerformed(ActionEvent e) {
        // this is one way to get hold of the top level container i.e. the GUIViewer JFrame
        GUIViewer viewer = (GUIViewer)this.getTopLevelAncestor();
        if (e.getActionCommand().equals("Read File")){
            // clear the display and append the staff
            
    
         viewer.getStaffDisplay().setText("");
         viewer.getStaffDisplay().append(Organisation.printStaff());
        }
        else if (e.getActionCommand().equals("Quit")){
            System.exit(0);
        }
       
        
        
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
