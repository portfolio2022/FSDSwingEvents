/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.kingston.ku63026.fsdswingeventlisteners.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import uk.ac.kingston.ku63026.fsdswingeventlisteners.view.GUIViewer;

/**
 *
 * @author dave
 */
public class Organisation {

    public static List<StaffMember> getStaffMembers() {
        return staffMembers;
    }

    public static void setStaffMembers(List<StaffMember> staffMembers) {
        staffMembers = staffMembers;
    }
    private static List<StaffMember> staffMembers = new ArrayList();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DataViewer d = new DataViewer();
        d.displayTasks();
        
        // TODO code application logic here
//        Organisation.readStaff("StaffMembersSmall.txt");
//        System.out.println(Organisation.getStaffMembers());
//        GUIViewer gv = new GUIViewer();
       // gv.getStaffDisplay().append(Organisation.printStaff());
    }
    
    public static String printStaff(){
        StringBuffer sb = new StringBuffer();
        for(StaffMember s: staffMembers){
         //   sb.append(s.toString()); improving layout
         // improve String formattting using String format
         // https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Formatter.html#syntax
         // experiment with a fixed width and monotype font for the text area definition see GUI Definition
         sb.append(String.format("%1$-5s%2$-30s%3$-10s", s.getId(), s.getName(), s.getJobTitle()));
         sb.append(System.lineSeparator());
        }
        return sb.toString();
    }
    
    public static void readStaff(String filename){
        try(BufferedReader br = new BufferedReader(new FileReader(filename))){
            while(br.ready()){
                StaffMember s = new StaffMember(br.readLine());
                staffMembers.add(s);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
