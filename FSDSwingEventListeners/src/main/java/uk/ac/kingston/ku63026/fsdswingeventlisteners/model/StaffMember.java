/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.kingston.ku63026.fsdswingeventlisteners.model;

/**
 *
 * @author dave
 */
public class StaffMember {
    
    private String id;
    
    private String name;
    
    private String jobTitle;
    
    private String lineManagerId;

    public StaffMember(String csvLine) {
        String[] attributes = csvLine.split(",");
        this.id = attributes[0];
        this.name = attributes[1];
        this.jobTitle = attributes[2];
        this.lineManagerId  = attributes[3];
    }
   
    /**
     * Get the value of lineManagerId
     *
     * @return the value of lineManagerId
     */
    public String getLineManagerId() {
        return lineManagerId;
    }

    /**
     * Set the value of lineManagerId
     *
     * @param lineManagerId new value of lineManagerId
     */
    public void setLineManagerId(String lineManagerId) {
        this.lineManagerId = lineManagerId;
    }

    /**
     * Get the value of jobTitle
     *
     * @return the value of jobTitle
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Set the value of jobTitle
     *
     * @param jobTitle new value of jobTitle
     */
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public String getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "StaffMember{" + "id=" + id + ", name=" + name + ", jobTitle=" + jobTitle + ", lineManagerId=" + lineManagerId + '}';
    }
 
    
    
}
