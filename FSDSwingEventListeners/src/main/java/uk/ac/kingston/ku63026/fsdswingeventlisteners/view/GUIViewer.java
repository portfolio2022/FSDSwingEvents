/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.kingston.ku63026.fsdswingeventlisteners.view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;
import uk.ac.kingston.ku63026.fsdswingeventlisteners.model.Organisation;

/**
 *
 * @author dave
 */
public class GUIViewer extends JFrame implements ActionListener{
    public static GUIViewer theOnlyOne;

    public static GUIViewer getTheOnlyOne() {
        return theOnlyOne;
    }

    public static void setTheOnlyOne(GUIViewer theOnlyOne) {
        GUIViewer.theOnlyOne = theOnlyOne;
    }
    
    public JTextArea getStaffDisplay() {
        return staffDisplay;
    }
    public JButton readFile = new JButton("Read Staff File");
    public JButton quit = new JButton("Quit");
   // public JPanel buttonPanel = new JPanel();
    public ButtonPanel buttonPanel = new ButtonPanel();
    
    // set up menu options 
    public JMenuBar mbar = new JMenuBar();
    public JMenu fileMenu = new JMenu("File");
    public JMenuItem openFile = new JMenuItem("Open File...");
    public JMenuItem quitItem = new JMenuItem("Quit");
    // start the file chooser in the NetBeans directory
    public JFileChooser csvChoose = new JFileChooser(System.getProperty("user.dir"));
    FileNameExtensionFilter csvFilter = new FileNameExtensionFilter(
        "CSV or Text Files", "csv", "txt");
    
    public void setStaffDisplay(JTextArea staffDisplay) {
        this.staffDisplay = staffDisplay;
    }
    JTextArea staffDisplay = new JTextArea();

    public GUIViewer() throws HeadlessException {
        theOnlyOne = this;
        this.setLayout(new BorderLayout());
       // buttonPanel.setLayout(new GridLayout(1,2));
      //  this.readFile.addActionListener(this);
      //  this.readFile.setActionCommand("Read File");
       // this.quit.addActionListener(this);
       // this.quit.setActionCommand("Quit");
        // use a MONOSPACED font so that everything lines up
        staffDisplay.setFont(new Font(Font.MONOSPACED,Font.PLAIN,12));
        this.add(staffDisplay,BorderLayout.CENTER);
        //buttonPanel.add(readFile);
       // buttonPanel.add(quit);
        
        this.add(buttonPanel, BorderLayout.SOUTH);
        
        
        // First setup the menu structure
        // each item needs an ActionListener (other Listeners are available!) 
        openFile.addActionListener(this);
        quitItem.addActionListener(this);
        // now an ActionCommand to identify what has happened in our catch all ActionPerformed method
        //at the moment these are the same as the Buttons
       // openFile.setActionCommand("Read File");
        openFile.setActionCommand("Open File");
        quitItem.setActionCommand("Quit");
        fileMenu.add(openFile);
        fileMenu.add(quitItem);
        mbar.add(fileMenu);
        // Now add to the JFrame - uses a setJMenuBar method
        this.setJMenuBar(mbar);
        
        this.setTitle("Staff Member Viewer");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getActionCommand().equals("Read File")){
            // clear the display and append the staff
            this.getStaffDisplay().setText("");
            this.getStaffDisplay().append(Organisation.printStaff());
        }
        else if (e.getActionCommand().equals("Quit")){
            System.exit(0);
        }
        else if (e.getActionCommand().equals("Open File")){
           // associate the file filter
            csvChoose.setFileFilter(csvFilter);
            int userResponse = csvChoose.showOpenDialog(this);
            if (userResponse == JFileChooser.APPROVE_OPTION){
                String filename = csvChoose.getSelectedFile().toString();
                Organisation.readStaff(filename);
                this.getStaffDisplay().append(Organisation.printStaff());
            }
        }
        
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
