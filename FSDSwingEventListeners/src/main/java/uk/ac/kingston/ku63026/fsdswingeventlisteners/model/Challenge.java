/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.kingston.ku63026.fsdswingeventlisteners.model;

import java.util.List;	 	        		 	      		      	
import java.util.ArrayList;	 	        		 	      		      	
import java.io.FileNotFoundException;	 	        		 	      		      	
import java.io.File;	 	        		 	      		      	
import java.util.Scanner;
	 	        		 	      		      	
public class Challenge	 	        		 	      		      	
{	 	        		 	      		      	
  private static List<String[]> tasks = new ArrayList();	 	        		 	      		      	
	 	        		 	      		      	
  public static void readCSVFile(String filename)	 	        		 	      		      	
  {	 	        		 	      		      	
    try	 	        		 	      		      	
    {	 	        		 	      		      	
    File myFile = new File(filename);	 	        		 	      		      	
    Scanner myScanner = new Scanner(myFile);	 	        		 	      		      	
    List<String[]> myTask = new ArrayList();	 	        		 	      		      	
    while(myScanner.hasNext())	 	        		 	      		      	
    {	 	        		 	      		      	
      String[] myCSV = myScanner.nextLine().split(",");
      String valueOne = myCSV[0];	 	        		 	      		      	
      String valueTwo = myCSV[1];	 	        		 	      		      	
      String valueThree = myCSV[2];
      String[] myValues = {valueOne,valueTwo,valueThree};
      myTask.add(myValues);	 	        		 	      		      	
    }	 	        		 	      		      	
    myScanner.close();	 	        		 	      		      	
    Challenge.setTasks(myTask);	 	        		 	      		      	
    } catch (FileNotFoundException e)	 	        		 	      		      	
    {	 	        		 	      		      	
      e.printStackTrace();	 	        		 	      		      	
    }	 	        		 	      		      	
  }	 	        		 	      		      	
	 	        		 	      		      	
  public static List<String[]> getTasks(){	 	        		 	      		      	
     return tasks;	 	        		 	      		      	
  }	 	        		 	      		      	
	 	        		 	      		      	
  public static void setTasks(List<String[]> tasks){	 	        		 	      		      	
     Challenge.tasks = tasks;	 	        		 	      		      	
  }	 	        		 	      		      	
}
