/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package uk.ac.kingston.ku63026.fsdswingeventlisteners.model;

import java.awt.BorderLayout;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Shadow
 */
public class DataViewer extends JFrame{
    
    private JFrame myFrame;
    private JTextArea myText;
    
    
    public DataViewer()
    {
        this.myFrame = new JFrame("DataViewer");
        this.myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.myFrame.setSize(500,500);
        this.myFrame.setVisible(true);
        this.myFrame.setLayout(new BorderLayout());
        this.myText = new JTextArea(20,10);
        JScrollPane myPanel = new JScrollPane(this.myText);
        myPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);  
        myPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);  
        this.myFrame.getContentPane().add(myPanel, BorderLayout.CENTER);
        
    }
    
    public void displayTasks()
    {
        Challenge c = new Challenge();
        c.readCSVFile("StaffMembers.txt");
        List<String[]> myTasks = c.getTasks();
        String myStrings = "ID                      Name                            Role" + System.lineSeparator();
        for (String[] nextTask : myTasks)
        {
            String taskTitle = nextTask[0];
            String taskPriority = nextTask[1];
            String taskDate = nextTask[2];
            String oneTask = taskTitle + "----" + taskPriority + "----" + taskDate + System.lineSeparator();
            myStrings += oneTask;
        }
        this.myText.setText(myStrings);
        
    }
    
}
